package testes;

import java.text.SimpleDateFormat;
import java.util.List;

import dominio.Acucar;
import dominio.Arroz;
import dominio.Leite;
import dominio.Produto;
import dominio.Receita;






public class Teste {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<Produto> historico;
		
		String nome = "Arroz de Leite";
		Arroz produto1 = new Arroz(100); 
		Acucar produto2 = new Acucar(20);
		Leite produto3 = new Leite(50);
		
		Receita receita1 = new Receita(nome, produto1);
		
		receita1.adicionarProduto(produto2);
		receita1.adicionarProduto(produto3);
		
	
        List<Produto> hist = receita1.getProdutos();
        System.out.println("Receita: " + receita1.getNome());
        System.out.println("Nome do Produto - Peso - Total Calorias por Item");
        
        for (Produto det: hist) {
        	System.out.printf("%s - %s - %s \n",det.getClass().getName(),det.getPeso(), det.caloriasPorGrama()*det.getPeso() + "g");
        }
        System.out.println("Peso Total: " + receita1.pesoTotal());
        System.out.println("Calorias Total: " + receita1.totalCalorias());
        
		

	}

}
