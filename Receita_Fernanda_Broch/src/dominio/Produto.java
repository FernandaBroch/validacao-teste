package dominio;

public abstract class Produto {
	
	private float peso;
	

	public Produto(float peso) {
		this.peso = peso;
	}
	
	public abstract float caloriasPorGrama();
	
	public float totalCalorias(){
		return (this.caloriasPorGrama() * this.peso);
	}

	public float getPeso() {
		return peso;
	}

	public void setPeso(float peso) {
		this.peso = peso;
	}



}
