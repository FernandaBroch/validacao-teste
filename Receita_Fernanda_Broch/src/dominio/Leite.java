package dominio;

public class Leite extends Produto {
	
	public Leite(float peso) {
		super(peso);
		
	}
	
	public float caloriasPorGrama(){
		return 2;
	}

}
